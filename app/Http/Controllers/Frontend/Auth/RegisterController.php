<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\engageme\Users\Models\Users;
use App\Http\Controllers\Controller;
use App\engageme\Users\Requests\RegisterRequest;
use App\engageme\Users\Repositories\UsersRepository;

class RegisterController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * RegisterController constructor.
     * @param UsersRepository $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    /**
     * Shows the register form.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handling for a register request.
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRegister(RegisterRequest $request)
    {
        $display_name = "$request->input('first_name') $request->input('last_name')";
        $user_name = str_slug($display_name);

        $data = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'display_name' => $display_name,
            'user_name' => $this->checkIfUserNameIsUnique($user_name),
        ];

        $this->usersRepository->createUser($data);

        return redirect()
            ->route('home');
    }

    /**
     * Check if the username is unique.
     * @param string $username
     * @return string
     */
    public function checkIfUserNameIsUnique($username)
    {
        if (!Users::where('user_name', $username)->exists()) {
            return $username;
        }

        $newUsername = $username;

        $users = Users::where('user_name', 'LIKE', $username.'%')->get();

        while ($users->contains('user_name', $newUsername)) {
            $usernameNumber = count($users);
            $newUsername = $username.$usernameNumber;
        }

        return $newUsername;
    }
}
