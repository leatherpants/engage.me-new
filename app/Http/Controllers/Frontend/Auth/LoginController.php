<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\engageme\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * Shows the login form.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handling for a login request.
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        // successful login
        if (auth()->attempt($details)) {
            return redirect()
                ->route('dashboard');
        }

        return redirect()
            ->route('login');
    }

    /**
     * Handling the logout request.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->logout();

        return redirect()
            ->route('home');
    }
}
