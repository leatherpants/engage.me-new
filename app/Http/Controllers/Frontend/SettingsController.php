<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Users\Repositories\UsersAvatarsRepository;

class SettingsController extends Controller
{
    /**
     * @var UsersAvatarsRepository
     */
    private $usersAvatarsRepository;

    /**
     * SettingsController constructor.
     * @param UsersAvatarsRepository $usersAvatarsRepository
     */
    public function __construct(UsersAvatarsRepository $usersAvatarsRepository)
    {
        $this->usersAvatarsRepository = $usersAvatarsRepository;
    }

    /**
     * Overview of user settings.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();

        return view('settings.index')
            ->with('user', $user);
    }

    /**
     * Gets the user profile picture and the image for the applications.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImages()
    {
        $user_id = auth()->id();
        $user = auth()->user();

        $user_image = $this->usersAvatarsRepository->getImageByUserIdAndTypeAndSize($user_id, UsersAvatarsRepository::user, 'original');

        $cv_image = $this->usersAvatarsRepository->getImageByUserIdAndTypeAndSize($user_id, UsersAvatarsRepository::cv, 'original');

        return view('settings.images')
            ->with('cv_image', $cv_image)
            ->with('user', $user)
            ->with('user_image', $user_image);
    }
}
