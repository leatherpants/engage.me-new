<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Companies\Requests\StoreCompaniesRequest;
use App\engageme\Companies\Repositories\CompaniesRepository;

class CompaniesController extends Controller
{
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    /**
     * CompaniesController constructor.
     * @param CompaniesRepository $companiesRepository
     */
    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->companiesRepository = $companiesRepository;
    }

    /**
     * Shows a companies list.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $companies = $this->companiesRepository->list();

        return view('companies.index')
            ->with('companies', $companies);
    }

    /**
     * Shows the form for creating new companies.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Storing process for new companies.
     * @param StoreCompaniesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCompaniesRequest $request)
    {
        $user_id = auth()->id();

        $data = [
            'name' => $request->input('name'),
            'slug' => str_slug($request->input('name')),
            'description' => $request->input('description'),
            'creator_id' => $user_id,
            'manager_id' => null,
        ];

        $this->companiesRepository->storeCompany($data);

        return redirect()
            ->route('companies');
    }
}
