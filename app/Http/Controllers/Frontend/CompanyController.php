<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Companies\Repositories\CompaniesRepository;

class CompanyController extends Controller
{
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    /**
     * CompanyController constructor.
     * @param CompaniesRepository $companiesRepository
     */
    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->companiesRepository = $companiesRepository;
    }

    /**
     * Get the company profile by slug.
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(string $slug)
    {
        $company = $this->companiesRepository->getCompanyBySlug($slug);

        if (!$company) {
            abort(404);
        }

        return view('company.profile')
            ->with('company', $company);
    }
}
