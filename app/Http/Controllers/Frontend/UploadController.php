<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function uploadProfileImage()
    {
        $user = auth()->user();

        return view('settings.upload.profile')
            ->with('user', $user);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileImageUpload(Request $request)
    {
        if ($request->hasFile('image')) {
            $user = auth()->user();

            $image = $request->file('image');
            $type = 'user';
            $extension = $image->getClientOriginalExtension();
            $filename = $user->user_name.'-'.$type.'-original.'.$extension;
            Storage::disk('public')->put($filename, File::get($image));

            return redirect()->back();
        }

        return redirect()->back();
    }
}
