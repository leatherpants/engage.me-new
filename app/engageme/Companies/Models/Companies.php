<?php

namespace App\engageme\Companies\Models;

use App\engageme\Users\Models\Users;
use Illuminate\Database\Eloquent\Model;

/**
 * App\engageme\Companies\Models\Companies.
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property int $creator_id
 * @property int|null $manager_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\engageme\Users\Models\Users|null $manager
 * @mixin \Eloquent
 */
class Companies extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'creator_id',
        'manager_id',
    ];

    /**
     * Gets the managed user for a company profile.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Users::class, 'manager_id', 'id');
    }
}
