<?php

namespace App\engageme\Companies\Repositories;

use App\engageme\Companies\Models\Companies;

class CompaniesRepository
{
    /**
     * @var Companies
     */
    private $companies;

    /**
     * CompaniesRepository constructor.
     * @param Companies $companies
     */
    public function __construct(Companies $companies)
    {
        $this->companies = $companies;
    }

    /**
     * Get data for the company profile by the slug.
     * @param string $slug
     * @return mixed
     */
    public function getCompanyBySlug(string $slug)
    {
        return $this->companies
            ->where('slug', $slug)
            ->first();
    }

    /**
     * List all companies.
     * @return mixed
     */
    public function list()
    {
        return $this->companies
            ->get();
    }

    /**
     * Store new companies.
     * @param array $data
     */
    public function storeCompany(array $data)
    {
        $company = new Companies();
        $company->name = $data['name'];
        $company->slug = $data['slug'];
        $company->description = $data['description'];
        $company->creator_id = $data['creator_id'];
        $company->manager_id = $data['manager_id'];
        $company->save();
    }
}
