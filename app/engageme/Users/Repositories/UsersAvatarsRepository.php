<?php

namespace App\engageme\Users\Repositories;

use App\engageme\Users\Models\UsersAvatars;

class UsersAvatarsRepository
{
    /**
     * @var UsersAvatars
     */
    private $avatars;

    /**
     * type.
     */
    const user = 1;

    /**
     * type.
     */
    const cv = 2;

    /**
     * UsersAvatarsRepository constructor.
     * @param UsersAvatars $avatars
     */
    public function __construct(UsersAvatars $avatars)
    {
        $this->avatars = $avatars;
    }

    /**
     * Get image of the user by its id, required type and size.
     * @param int $user_id
     * @param int $type
     * @param string $size
     * @return mixed
     */
    public function getImageByUserIdAndTypeAndSize($user_id, $type, $size)
    {
        return $this->avatars
            ->where('user_id', $user_id)
            ->where('type', $type)
            ->where('size', $size)
            ->first();
    }
}
