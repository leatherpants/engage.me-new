<?php

namespace App\engageme\Users\Repositories;

use App\engageme\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Create new users.
     * @param array $data
     * @return Users
     */
    public function createUser($data)
    {
        $user = new Users();
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->display_name = $data['display_name'];
        $user->user_name = $data['user_name'];
        $user->save();

        return $user;
    }
}
