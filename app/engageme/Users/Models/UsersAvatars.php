<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\engageme\Users\Models\UsersAvatars.
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property string $size
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @mixin \Eloquent
 */
class UsersAvatars extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'size',
        'path',
    ];
}
