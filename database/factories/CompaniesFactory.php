<?php

use Faker\Generator as Faker;

$factory->define(\App\engageme\Companies\Models\Companies::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'description' => $faker->text,
        'creator_id' => function() {
            return factory(\App\engageme\Users\Models\Users::class)->create()->id;
        },
        'manager_id' => function() {
            return factory(\App\engageme\Users\Models\Users::class)->create()->id;
        },
    ];
});
