<?php

namespace Tests;

use App\engageme\Users\Models\Users;
use App\engageme\Companies\Models\Companies;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected $company;
    protected $user;

    /**
     * Set up the test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->company = factory(Companies::class)->create();
        $this->user = factory(Users::class)->create();
    }

    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
