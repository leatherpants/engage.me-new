<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingsTest extends TestCase
{
    /** @test */
    public function test_shows_user_settings_index()
    {
        $response = $this->actingAs($this->user)->get(route('settings'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_user_images_settings()
    {
        $response = $this->actingAs($this->user)->get(route('settings.images'));
        $response->assertSuccessful();
    }
}
