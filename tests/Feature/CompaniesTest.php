<?php

namespace Tests\Feature;

use Tests\TestCase;

class CompaniesTest extends TestCase
{
    /** @test */
    public function test_shows_companies_list()
    {
        $response = $this->actingAs($this->user)->get(route('companies'));
        $response->assertSuccessful();

        $response->assertSee('Companies');
    }

    /** @test */
    public function test_shows_companies_creation_form()
    {
        $response = $this->actingAs($this->user)->get(route('companies.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_stores_new_companies()
    {
        $data = [
            'name' => 'Scuderia Ferrari',
            'slug' => 'scuderia-ferrari',
            'description' => 'Scuderia Ferrari',
            'creator_id' => 1,
            'manager_id' => null,
        ];

        $response = $this->actingAs($this->user)->post(route('companies.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('companies'));
    }
}
