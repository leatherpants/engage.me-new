<?php

namespace Tests\Feature;

use Tests\TestCase;

class CompanyTest extends TestCase
{
    /** @test */
    public function test_shows_company_profile_with_manager()
    {
        $data = [
            'manager_id' => 1,
        ];

        $response = $this->actingAs($this->user)->get(route('company', ['slug' => $this->company->slug]), $data);
        $response->assertSuccessful();

        $this->assertNotEmpty($data['manager_id']);
    }

    /** @test */
    public function test_shows_company_profile_without_manager()
    {
        $data = [
            'manager_id' => null,
        ];

        $response = $this->actingAs($this->user)->get(route('company', ['slug' => $this->company->slug]), $data);
        $response->assertSuccessful();

        $this->assertEmpty($data['manager_id']);
    }

    /** @test */
    public function test_shows_company_profile_fails()
    {
        $response = $this->actingAs($this->user)->get(route('company', ['slug' => 'company']));
        $response->assertNotFound();
    }
}
