<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @test */
    public function test_shows_login_form()
    {
        $response = $this->get(route('login'));
        $response->assertSuccessful();

        $response->assertSee('Email');
        $response->assertSee('Password');
        $response->assertSee('Login');
        $response->assertSee('Register');
    }

    /** @test */
    public function test_shows_register_form()
    {
        $response = $this->get(route('register'));
        $response->assertSuccessful();

        $response->assertSee('Email');
        $response->assertSee('Password');
        $response->assertSee('First name');
        $response->assertSee('Last name');
        $response->assertSee('Register');
    }

    /** @test */
    public function test_successful_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'secret']);
        $response->assertStatus(302);
        $response->assertRedirect(route('dashboard'));
    }

    /** @test */
    public function test_successful_register()
    {
        $data = [
            'email' => 'test@test.com',
            'password' => 'secret',
            'first_name' => 'John',
            'last_name' => 'Doe',
        ];

        $response = $this->post(route('register'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_checks_if_username_is_unique()
    {
        $data1 = [
            'email' => 'test@test.com',
            'password' => 'secret',
            'first_name' => 'John',
            'last_name' => 'Doe',
        ];

        $data2 = [
            'email' => 'test1@test.com',
            'password' => 'secret',
            'first_name' => 'Lisa',
            'last_name' => 'Doe',
        ];

        $response = $this->post(route('register'), $data1);
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));

        $response = $this->post(route('register'), $data2);
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_failed_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'xxxyyy']);
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function test_access_to_the_user_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('dashboard'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_successful_logout()
    {
        $response = $this->actingAs($this->user)->get(route('logout'));
        $response->assertStatus(302);
        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function test_access_to_dashboard()
    {
        $response = $this->get('home');
        $response->assertRedirect('login');
    }
}
