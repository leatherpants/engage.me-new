<?php

namespace Tests\Feature;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_shows_landing_page()
    {
        $response = $this->get(route('home'));
        $response->assertSuccessful();

        $response->assertSee('Login');
        $response->assertSee('Register');
    }
}
