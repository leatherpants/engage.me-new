<?php

return [
    'companies' => 'Companies',
    'email' => 'Email',
    'first_name' => 'First name',
    'images' => 'Images',
    'job' => 'Job',
    'last_name' => 'Last name',
    'login' => 'Login',
    'logout' => 'Logout',
    'member_since' => 'Member since',
    'name' => 'Name',
    'password' => 'Password',
    'register' => 'Register',
    'settings' => 'Settings',
    'unknown' => 'unknown',
    'upload' => 'Upload',
    'your_settings' => 'Your settings',
    'your_settings_text' => ':first_name, you decide about your data in your account.',
];
