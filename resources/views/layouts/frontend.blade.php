<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="*Placeholder*{{--TODO: Find the right description--}}">
    <link href="{{ asset('assets/semantic.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/style.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .ui.menu .item img.logo {
            margin-right: 1.5em;
        }
        .main.container {
            margin-top: 7em;
        }
        .ui.footer.segment {
            margin: 5em 0 0;
            padding: 5em 0;
        }
        @yield('styles')
    </style>
</head>
<body>
<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="{{ route('dashboard') }}" class="header item">
            engage.me
        </a>
        <a href="{{ route('companies') }}" class="item">{{ trans('common.companies') }}</a>
        <div class="right menu">
            <div class="item">{{ auth()->user()->first_name }}</div>
            <a href="{{ route('settings') }}" title="{{ trans('common.settings') }}" class="icon item">
                <i class="settings icon"></i>
            </a>
            <a href="{{ route('logout') }}" title="{{ trans('common.logout') }}" class="icon item">
                <i class="sign out icon"></i>
            </a>
        </div>
    </div>
</div>

<div class="ui main container">
@yield('content')
</div>

<div class="ui inverted vertical footer segment">
    <div class="ui center aligned container">
        <div class="ui stackable inverted divided grid">
            <div class="three wide column">
                <h4 class="ui inverted header">Group 1</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Link One</a>
                    <a href="#" class="item">Link Two</a>
                    <a href="#" class="item">Link Three</a>
                    <a href="#" class="item">Link Four</a>
                </div>
            </div>
            <div class="three wide column">
                <h4 class="ui inverted header">Group 2</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Link One</a>
                    <a href="#" class="item">Link Two</a>
                    <a href="#" class="item">Link Three</a>
                    <a href="#" class="item">Link Four</a>
                </div>
            </div>
            <div class="three wide column">
                <h4 class="ui inverted header">Group 3</h4>
                <div class="ui inverted link list">
                    <a href="#" class="item">Link One</a>
                    <a href="#" class="item">Link Two</a>
                    <a href="#" class="item">Link Three</a>
                    <a href="#" class="item">Link Four</a>
                </div>
            </div>
            <div class="seven wide column">
                <h4 class="ui inverted header">Footer Header</h4>
                <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
            </div>
        </div>
        <div class="ui inverted section divider"></div>
        <div class="ui horizontal inverted small divided link list">
            <a class="item" href="#">Site Map</a>
            <a class="item" href="#">Contact Us</a>
            <a class="item" href="#">Terms and Conditions</a>
            <a class="item" href="#">Privacy Policy</a>
        </div>
    </div>
</div>
    <script src="{{ asset('assets/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/semantic.min.js') }}"></script>
@yield('scripts')
</body>
</html>