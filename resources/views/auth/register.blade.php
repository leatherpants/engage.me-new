@extends('layouts.blank')
@section('title', 'engage.me')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui orange header">
                <span class="content">
                    engage.me
                </span>
            </h2>
            <form action="{{ route('register') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="{{ $errors->has('first_name') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="first_name" id="first_name" placeholder="{{ trans('common.first_name') }}" value="{{ old('first_name') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('last_name') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="last_name" id="last_name" placeholder="{{ trans('common.last_name') }}" value="{{ old('last_name') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('email') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="email" name="email" id="email" placeholder="{{ trans('common.email') }}" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="{{ $errors->has('password') ? 'error ' : '' }}field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" id="password" placeholder="{{ trans('common.password') }}">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large orange submit button">{{ trans('common.register') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('styles')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
    </style>
@endsection