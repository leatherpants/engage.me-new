@extends('layouts.frontend')
@section('title', 'engage.me - '.$company->slug)
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui inverted dividing orange header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ $company->name }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ten wide column">
                <div class="ui inverted segment">
                    <p class="header">{{ trans('companies.description') }}</p>
                    <p>{{ $company->description }}</p>
                </div>
            </div>
            <div class="six wide column">
                <div class="ui inverted segment">
                    @if (!is_null($company->manager_id))
                    <p>This profile is managed by {{ $company->manager->display_name }}.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection