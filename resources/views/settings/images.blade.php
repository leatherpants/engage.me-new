@extends('layouts.frontend')
@section('title', 'engage.me - Settings')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui dividing inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.your_settings') }}
                            <span class="sub header">{{ trans('common.your_settings_text', ['first_name' => $user->first_name]) }}</span>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('settings._partials.header')
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <div class="ui stackable two column grid">
                        <div class="four wide column">
                            @include('settings._partials.menu')
                        </div>
                        <div class="twelve wide column">
                            <div class="ui stackable grid">
                                <div class="eight wide column">
                                    <h3 class="ui dividing inverted header">
                                        profile image
                                    </h3>
                                    @if (!is_null($user_image))

                                        <div class="ui image">
                                            <img src="{{ $user_image->path }}" alt="">
                                        </div>
                                    @else

                                        <div class="ui placeholder" style="max-width: 100%;">{{--TODO:Style--}}
                                            <div class="image"></div>
                                        </div>
                                    @endif

                                    <a href="{{ route('upload.profileImage') }}" class="ui inverted orange fluid button" style="margin-top: 1rem;">{{--TODO:Style--}}
                                        <i class="upload icon"></i> {{ trans('common.upload') }}
                                    </a>
                                </div>
                                <div class="eight wide column">
                                    <h3 class="ui dividing inverted header">
                                        application image
                                    </h3>
                                    @if (!is_null($cv_image))

                                        <div class="ui image">
                                            <img src="{{ $cv_image->path }}" alt="">
                                        </div>
                                    @else

                                        <div class="ui placeholder" style="max-width: 100%;">{{--TODO:Style--}}
                                            <div class="image"></div>
                                        </div>
                                    @endif

                                    <div class="ui inverted orange fluid button" style="margin-top: 1rem;">{{--TODO:Style--}}
                                        <i class="upload icon"></i> {{ trans('common.upload') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection