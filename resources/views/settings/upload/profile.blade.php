@extends('layouts.frontend')
@section('title', 'engage.me - Settings')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui dividing inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.your_settings') }}
                            <span class="sub header">{{ trans('common.your_settings_text', ['first_name' => $user->first_name]) }}</span>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('settings._partials.header')
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <form action="{{ route('upload.profileImage.process') }}" enctype="multipart/form-data" method="post" class="ui inverted form">
                        @csrf

                        <input type="file" name="image" class="ui inverted button" id="uploadbutton" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
                        <label for="uploadbutton" class="ui orange icon button">
                            <i class="upload icon"></i>
                        </label>
                        <button type="submit" class="ui orange button">
                            <i class="save icon"></i>
                            {{ trans('common.upload') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection