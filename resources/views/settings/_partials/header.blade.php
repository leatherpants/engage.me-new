<div class="ui inverted segment">
    <img src="https://media.licdn.com/dms/image/C5603AQHGL6hwaLbccw/profile-displayphoto-shrink_200_200/0?e=1544659200&v=beta&t=991-zW_u3VTWe5MM9to3iK9VrmLRQX-Xjf4J9bm_lHI" alt="{{ $user->display_name }}" class="ui small left floated image" style="margin-bottom: 0;">
    <div class="ui inverted list" style="margin: 0;">{{--TODO:Modify style--}}
        <div class="item">
            <div class="content">
                <div class="header">{{ trans('common.name') }}</div>
                <div class="description">{{ $user->display_name }}</div>
            </div>
            <div class="content">
                <div class="header">{{ trans('common.job') }}</div>
                <div class="description">{{ trans('common.unknown') }}</div>
            </div>
            <div class="content">
                <div class="header">{{ trans('common.member_since') }}</div>
                <div class="description">{{ $user->created_at->diffForHumans() }} <small>({{ $user->created_at->format('d.m.y H:i') }})</small></div>
            </div>
        </div>
    </div>
</div>