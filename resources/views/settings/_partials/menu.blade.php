<div class="ui inverted secondary vertical pointing menu">
    <a href="{{ route('settings') }}" class="{{ Route::is('settings') ? 'active ' : '' }}item">
        {{ trans('common.settings') }}
    </a>
    <a href="{{ route('settings.images') }}" class="{{ Route::is('settings.images') ? 'active ' : '' }}item">
        {{ trans('common.images') }}
    </a>
</div>