@extends('layouts.frontend')
@section('title', 'engage.me - Settings')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui dividing inverted orange header">
                        <i class="settings icon"></i>
                        <span class="content">
                            {{ trans('common.your_settings') }}
                            <span class="sub header">{{ trans('common.your_settings_text', ['first_name' => $user->first_name]) }}</span>
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                @include('settings._partials.header')
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <div class="ui stackable two column grid">
                        <div class="four wide column">
                            @include('settings._partials.menu')
                        </div>
                        <div class="column">x</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection