@extends('layouts.frontend')
@section('title', 'engage.me - Companies')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui inverted dividing orange header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ trans('common.companies') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    @if (count($companies))
                    <div class="ui inverted divided items">
                        @foreach($companies as $company)
                            <div class="item">
                                <div class="content">
                                    <a href="{{ route('company', ['slug' => $company->slug]) }}" class="header">{{ $company->name }}</a>
                                    <div class="meta">
                                        <span>Category</span>
                                    </div>
                                    <div class="description">
                                        Description
                                    </div>
                                    <div class="extra">
                                        City, Country
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @else
                    <div class="ui icon inverted message">
                        <i class="times circle icon"></i>
                        <div class="content">
                            <div class="header">No companies available.</div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection