@extends('layouts.frontend')
@section('title', 'engage.me')
@section('content')
    <div class="ui stackable grid">
        <div class="row">
            <div class="column">
                <div class="ui inverted segment">
                    <h3 class="ui inverted dividing orange header">
                        <i class="building icon"></i>
                        <span class="content">
                            {{ trans('companies.create_companies') }}
                        </span>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="column">
                <div class="ui inverted clearing segment">
                    <form action="{{ route('companies.create') }}" method="post" class="ui inverted form">
                        @csrf

                        <div class="field">
                            <label for="name">{{ trans('companies.name') }}</label>
                            <input type="text" name="name" id="name" placeholder="{{ trans('companies.name') }}" value="{{ old('name') }}">
                        </div>
                        <div class="field">
                            <label for="description">{{ trans('companies.description') }}</label>
                            <textarea name="description" id="description" cols="30" rows="10" placeholder="{{ trans('companies.description') }}"></textarea>
                        </div>
                        <button class="ui right floated inverted labeled icon orange button">
                            <i class="save icon"></i>
                            {{ trans('companies.create') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection