<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'home']);

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@getLogin', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@postLogin']);
        Route::get('register', ['uses' => 'RegisterController@getRegister', 'as' => 'register']);
        Route::post('register', ['uses' => 'RegisterController@postRegister']);
        Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('home', ['uses' => 'HomeController@dashboard', 'as' => 'dashboard']);

        Route::group(['prefix' => 'companies'], function () {
            Route::get('', ['uses' => 'CompaniesController@index', 'as' => 'companies']);
            Route::get('create', ['uses' => 'CompaniesController@create', 'as' => 'companies.create']);
            Route::post('create', ['uses' => 'CompaniesController@store']);
        });

        Route::group(['prefix' => 'company'], function () {
            Route::get('{slug}', ['uses' => 'CompanyController@profile', 'as' => 'company']);
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', ['uses' => 'SettingsController@index', 'as' => 'settings']);
            Route::get('images', ['uses' => 'SettingsController@getImages', 'as' => 'settings.images']);

            Route::group(['prefix' => 'upload'], function () {
                Route::get('profile', ['uses' => 'UploadController@uploadProfileImage', 'as' => 'upload.profileImage']);
                Route::post('profile', ['uses' => 'UploadController@profileImageUpload', 'as' => 'upload.profileImage.process']);
            });
        });
    });
});
